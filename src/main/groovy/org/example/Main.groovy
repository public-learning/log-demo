package org.example

import ch.qos.logback.classic.Level
import org.slf4j.Logger
import org.slf4j.LoggerFactory

static void main(String[] args) {
    println "Hello world!"

    def testLogger = LoggerFactory.getLogger("org.example")
    testLogger.trace("Trace message")
    testLogger.debug("Debug message")
    testLogger.info("Info message")
    testLogger.error("Error message")
}
